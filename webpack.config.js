var path = require('path');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var DashboardPlugin = require('webpack-dashboard/plugin');

module.exports = {
    entry: './app/main.js',
    output: {
        path: path.join(__dirname, 'app'),
        filename: 'bundle.js',
        publicPath: '/app/'
    },
    resolve: {
        extensions: [".js", ".json", ".jsx"]
    },
    devServer: {
        inline: true,
        contentBase: './app',
        port: 8100,
        historyApiFallback: true
    },
    module: {
        rules: [{
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: ['babel-loader']
            },
            {
                test: /\.scss$/,
                loaders: ['style-loader', 'css-loader', 'sass-loader']
            }
        ]
    },
    plugins: [
        new CopyWebpackPlugin([
            { from: './app/assets', to: 'assets' }
        ]),
        new DashboardPlugin([
            {port: 8100}
        ])
    ],

}
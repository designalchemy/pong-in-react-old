import React, {Component} from 'react'

import css from './Pitch.scss'

class Pitch extends Component {

	componentDidMount() {

		const width = document.getElementsByClassName('pitch-container__mask')[0].offsetWidth
		const height = document.getElementsByClassName('pitch-container__mask')[0].offsetHeight

		this.props.dimentions(width, height)
		
	}

    render() {

        return (
            <div className="pitch-container">

                <div className="pitch-container__mask">

                    {this.props.children}

                </div>

            </div>
        )

    }

}

export default Pitch

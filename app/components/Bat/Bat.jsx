import React, {Component} from 'react'

import css from './Bat.scss'

class Bat extends Component {

    render() {

    	const position = this.props.pos == 'left' ? 'left' : 'right'
    	const classes = position + ' bat-container'

        return (
            <div className={classes} style={{top: this.props.top}} />
        )

    }

}

export default Bat

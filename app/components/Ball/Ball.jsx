import React, {Component} from 'react'

import css from './Ball.scss'

class Ball extends Component {

	constructor(props) {
	    super(props)
	}

    render() {

        return (
            <div className="ball-container" style={{left: this.props.left, top: this.props.top}} />
        )

    }

}

export default Ball

import React, {Component} from 'react'

import Pitch from '../Pitch/Pitch'
import Bat from '../Bat/Bat'
import Ball from '../Ball/Ball'

import css from './Index.scss'

class Index extends Component {

    constructor(props) {
        super(props)

        this.state = {
            batHeight: 50,
            moveSpeed: 30,
            ballSpeed: 15,
        	width: 0,
            height: 0,
            ballX: 0,
            ballY: 50,
            ballYAngle: 3,
            playTop: 0,
            cpuTop: 40,
            forwards: true,
            playerScore: 0,
            cpuScore: 0
        }

        this.setDimentions = this.setDimentions.bind(this)
        this.ballTimer = this.ballTimer.bind(this)
    }

    componentDidMount() {
        document.addEventListener('keydown', this.handleKeyDown.bind(this))
    }

    handleKeyDown(e) {
        switch(e.keyCode) {
            case 40:
                this.moveBat('up')
                break;
            case 38:
                this.moveBat('down')
                break;
            case 32:
                this.startGame()
                break;
            default: 
                break;
        }
    }

    startGame() {

        this.setState({
            ballX: this.state.width / 2,
            ballY: this.state.height / 2
        })

        this.ballTimer()

    }

    setDimentions(width, height) {
        this.setState({
            width: width,
            height: height
        })
    }

    moveBat(direction) {
        const playTopState = this.state.playTop
        const moveSpeed = this.state.moveSpeed

        if (direction == 'up') {
            if (playTopState + this.state.batHeight + moveSpeed  <= this.state.height) {
                this.setState({playTop: playTopState + moveSpeed})
            }
        } else {
            if (playTopState - moveSpeed >= 0) {
                this.setState({playTop: playTopState - moveSpeed})
            }
        }
    }

    ballTimer() {

        this.ballTimerInterval = setInterval(() => {

            this.moveBallX()
            this.moveCpu()

        }, this.state.ballSpeed)


    }

    moveBallX() {

        const { ballX, ballY, width, height, batHeight, playTop, cpuTop, ballYAngle } = this.state

        const moveAmmount = 5
        const ballWidth = 10

        if (ballY + ballWidth >= height) {
            this.setState({ballYAngle: -3})
        }

        if (ballY <= 0) {
            this.setState({ballYAngle: 3})
        }

        if (this.state.forwards) {

            // right to left
            if (ballX + moveAmmount + ballWidth > width) {

                if (ballY > cpuTop && ballY < cpuTop + batHeight) {

                    // hit back
                    this.setState({
                        forwards: false,
                        ballX: ballX - moveAmmount,
                        ballY: ballY + ballYAngle
                    })

                } else {

                    clearInterval(this.ballTimerInterval)
                    this.scorePoint('left')
                    console.log('player 1 scored')
                }

            } else { 

                // move 
                this.setState({
                    ballX: ballX + moveAmmount,
                    ballY: ballY + ballYAngle
                })

            }

        } else {

            // left to right
            if (ballX - moveAmmount < 0) {

                if (ballY > playTop && ballY < playTop + batHeight) {

                    // hit back
                    this.setState({
                        forwards: true,
                        ballX: ballX + moveAmmount,
                        ballY: ballY + ballYAngle
                    })

                } else {

                    clearInterval(this.ballTimerInterval)
                    this.scorePoint('right')
                    console.log('player 2 scored')
                }

            } else {

                // move
                this.setState({
                    ballX: ballX - moveAmmount,
                    ballY: ballY + ballYAngle
                })

            }

        }

    }

    moveCpu() {

        this.setState({
            cpuTop: this.state.ballY - 25
        })

    }

    scorePoint(side) {

        if (side == 'left') {
            this.setState({playerScore: this.state.playerScore + 1})
        } else {
            this.setState({cpuScore: this.state.cpuScore + 1})
        }

    }

    render() {

        return (
            <div className="">

                <Pitch dimentions={this.setDimentions}>

                    <div className="score-board left">{this.state.playerScore}</div>

                    <div className="score-board right">{this.state.cpuScore}</div>

                    <Bat pos="left" top={this.state.playTop} />

                    <Bat pos="right" top={this.state.cpuTop} />

                    <Ball left={this.state.ballX} top={this.state.ballY} />

                </Pitch>

            </div>
        )

    }

}

export default Index

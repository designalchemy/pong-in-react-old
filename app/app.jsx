import React, { Component } from 'react'

import css from './app.scss'

class App extends Component {

    constructor(props) {
        super(props)

        this.state = {
        	data: []
        }
    }

    render() {

    	// this is a renderer for the router, can also be used to pass data around

        return (
            <div className="">
            	{React.cloneElement(this.props.children, { data: this.state.json })}
            </div>
        )

    }

}

export default App

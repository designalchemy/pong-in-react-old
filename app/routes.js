import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import App from './app';
import Index from './components/Index/Index';

const Routes = () => {

	return (

    <Router history={browserHistory}>

			<Route path="*" component={App}>

				<IndexRoute component={Index} />

			</Route>

    </Router>

	)

}

export default Routes;
